<?php

namespace Itwmw\Validate\Middleware\Laravel;

use Closure;
use Illuminate\Http\Request;
use Itwmw\Validate\Middleware\ValidateMiddlewareConfig;

class ValidateMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if (!strpos($request->route()->getActionName(), '@')) {
            $controller = $request->route()->getActionName();
            if (!class_exists($controller)) {
                return $next($request);
            }
            $scene = '__invoke';
        } else {
            list($controller, $scene) = explode('@', $request->route()->getActionName());
        }

        $validators = ValidateMiddlewareConfig::instance()->getValidateFactory()->getValidate($controller, $scene);

        if ($validators) {
            $requestData = $request->all();
            if (!is_array($validators)) {
                $validators = [$validators];
            }

            $allData = [];

            foreach ($validators as $validator) {
                $data    = $validator->check($requestData);
                $allData = array_merge([], $allData, $data);
            }

            $request->offsetSet('__validate__data__', $allData);
        }

        return $next($request);
    }
}
