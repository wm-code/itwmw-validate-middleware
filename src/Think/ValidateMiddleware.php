<?php

namespace Itwmw\Validate\Middleware\Think;

use Closure;
use Itwmw\Validate\Middleware\ValidateMiddlewareConfig;
use think\App;
use think\Request;
use think\Response;
use W7\Validate\Exception\ValidateException;

class ValidateMiddleware
{
    protected $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * @param Request $request
     * @param Closure $next
     * @return Response
     * @throws ValidateException
     */
    public function handle(Request $request, Closure $next): Response
    {
        $controller = $request->controller();
        $scene      = $request->action();

        $suffix          = $this->app->config->get('route.controller_suffix') ? 'Controller' : '';
        $controllerLayer = $this->app->config->get('route.controller_layer') ? 'controller' : '';

        $class = $this->app->parseClass($controllerLayer, $controller . $suffix);

        $validators = ValidateMiddlewareConfig::instance()->getValidateFactory()->getValidate($class, $scene);

        if ($validators) {
            $requestData = $request->all();
            if (!is_array($validators)) {
                $validators = [$validators];
            }

            $allData = [];

            foreach ($validators as $validator) {
                $data    = $validator->check($requestData);
                $allData = array_merge([], $allData, $data);
            }

            $request->__validate__data__ = $allData;
        }

        return $next($request);
    }
}
